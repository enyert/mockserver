package com.everis.mockserver.service;


import com.everis.mockserver.model.GeneralDataRequest;
import com.everis.mockserver.model.GeneralDataResponse;

/**
 * Created by evinasgu - Everis SKL-1 on 15/03/2018.
 */
public interface GeneralDataService {
    GeneralDataResponse generateResponse(GeneralDataRequest request);
}
