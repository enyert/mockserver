package com.everis.mockserver;

import com.everis.mockserver.model.ContactExample;
import com.everis.mockserver.util.DataLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;


//@SpringBootApplication
public class MockserverApplication {

	public static void main(String[] args) throws IOException {
		//SpringApplication.run(MockserverApplication.class, args);
        DataLoader dataLoader = new DataLoader();
        //System.out.println(dataLoader.getContactExampleList().size());
        for(ContactExample item: dataLoader.getContactExampleList()) {
            System.out.println(item.toString());
        }
	}
}
