package com.everis.mockserver.util;

import com.everis.mockserver.model.ContactExample;
import com.everis.mockserver.model.DataFileInfo;
import com.everis.mockserver.model.DataFileInfoWrapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.boot.json.GsonJsonParser;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.beanio.*;

/**
 * Created by evinasgu - Everis SKL-1 on 15/03/2018.
 */
public class DataLoader {

    private DataFileInfoWrapper infoWrapper;

    private List<ContactExample> contactExampleList;

    public DataLoader() throws IOException {
        StreamFactory factory = StreamFactory.newInstance();

        this.infoWrapper = new DataFileInfoWrapper(new ArrayList<>());
        this.contactExampleList = new ArrayList<>();
        ClassLoader classLoader = getClass().getClassLoader();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(classLoader.getResource("data.json").getPath()));
        factory.load(classLoader.getResource("contact_format.xml").getPath());

        Object record = null;


        Gson gson = new Gson();
        this.infoWrapper = gson.fromJson(bufferedReader, (Type) DataFileInfoWrapper.class);

        BeanReader in = factory.createReader("contacts", new File(this.infoWrapper.getInfoList().get(0).getUrl()));

        while((record = in.read()) != null) {

            if("header".equals(in.getRecordName())) {
                Map<String,Object> header = (Map<String,Object>) record;
                System.out.println(header.get("fileDate"));
            } else if ("contact".equals(in.getRecordName())) {
                ContactExample contactExample = (ContactExample) record;
                this.contactExampleList.add(contactExample);
            } else if ("trailer".equals(in.getRecordName())) {
                Integer recordCount = (Integer) record;
                System.out.println(recordCount + " contacts processed");
            }
        }


        bufferedReader.close();
    }

    public DataFileInfoWrapper getInfoList() {
        return this.infoWrapper;
    }

    public List<ContactExample> getContactExampleList() {
        return this.contactExampleList;
    }
}
