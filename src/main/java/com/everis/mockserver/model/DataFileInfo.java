package com.everis.mockserver.model;

/**
 * Created by evinasgu - Everis SKL-1 on 15/03/2018.
 */
public class DataFileInfo {
    private String idc;
    private String documentNumber;
    private String url;

    public DataFileInfo(String idc, String documentNumber, String url) {
        this.idc = idc;
        this.documentNumber = documentNumber;
        this.url = url;
    }

    public String getIdc() {
        return idc;
    }

    public void setIdc(String idc) {
        this.idc = idc;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
