package com.everis.mockserver.model;

import java.util.List;

/**
 * Created by evinasgu - Everis SKL-1 on 15/03/2018.
 */
public class DataFileInfoWrapper {
    private List<DataFileInfo> infoList;

    public DataFileInfoWrapper(List<DataFileInfo> infoList) {
        this.infoList = infoList;
    }

    public List<DataFileInfo> getInfoList() {
        return infoList;
    }

    public void setInfoList(List<DataFileInfo> infoList) {
        this.infoList = infoList;
    }
}
