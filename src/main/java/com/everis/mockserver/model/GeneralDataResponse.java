package com.everis.mockserver.model;

public class GeneralDataResponse {
    private String prueba;

    public GeneralDataResponse(String prueba) {
        this.prueba = prueba;
    }

    public String getPrueba() {
        return prueba;
    }

    public void setPrueba(String prueba) {
        this.prueba = prueba;
    }
}
