package com.everis.mockserver.model;

/**
 * Created by evinasgu - Everis SKL-1 on 12/03/2018.
 */
public class GeneralDataRequest {
    private String idc;
    private String documentNumber;

    public GeneralDataRequest(String idc, String documentNumber) {
        this.idc = idc;
        this.documentNumber = documentNumber;
    }

    public String getIdc() {
        return idc;
    }

    public void setIdc(String idc) {
        this.idc = idc;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }
}
